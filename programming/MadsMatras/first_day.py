Exercise 1: What is the function of the secondary memory in a com puter? 
a) Execute all of the computation and logic of the program 
b) Retrieve web pages over the Internet 
c) Store information for the long term, even beyond a power cycle 
d) Take input from the user 
Answer c is correct

Exercise 2: What is a program? 
is a set of instructions, written in code

Exercise 3: What is the difference between a compiler and an inter preter? 
a compiler makes an irreversible executable file that runs the code, an interpreter run the code directly from the source file.

Exercise 4: Which of the following contains “machine code”? 

a) The Python interpreter 


Exercise 5: What is wrong with the following code:
1.14. EXERCISES 17 
>>> primt 'Hello world!' 
File "<stdin>", line 1 
primt 'Hello world!' 
^ 
SyntaxError: invalid syntax 
>>> 
the code is supposed to say print instead of primt

Exercise 6: Where in the computer is a variable such as “x” stored after the following Python line finishes? 
x = 123 

b) Main Memory 


Exercise 7: What will the following program print out: 
x = 43 
x = x + 1 
print(x) 

b) 44 




Exercise 8: Explain each of the following using an example of a human capability: (1) Central processing unit, (2) Main Memory, (3) Secondary Memory, (4) Input Device, and (5) Output Device. 
What is the human equivalent to a Central Processing Unit ? the brain.
What is the human equivalent to a main memory ? short term memory, instingst 
What is the human equivalent to a secondary memory ? long term memory, notes 
What is the human equivalent to an input device? your sensory memory
What is the human equivalent to an output device? voice


Exercise 9: How do you fix a “Syntax Error”?
read you code and edit the mistakes 

