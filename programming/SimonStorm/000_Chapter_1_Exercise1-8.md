### Exercise 1
What is the function of the secondary memory in a computer?

Storing data long term. Retains data even after power is turned off.

### Exercise 2
What is a program?

A program is a set of instructions that performs a specified task.

### Exercise 3
What is the differense between a interpreter and a compiler?

The interpreter reads every single line of code while the compiler need a entire program in a file

### Exercise 4
Which of the following contains "Machine Code"?

C) Python source file

### Exercise 5
What is wrong with the following code?
```python
 >>> primt 'Hello world!'
File "<stdin>", line 1
primt 'Hello world!'

SyntaxError: invalid syntax
>>>
```
primt -> print and Missing parenthesis

### Exercise 6
Where in the computer is a variable such as “x” stored after the following Python line finishes?

x = 123

b) Main memory

### Exercise 7
What will the following program print out:
```python
x = 43
x = x + 1
print(x)
```

b) 44

### Exercise 8
Explain each of the following using an example of a human capability: (1) Central processing unit, (2) Main Memory, (3)
Secondary Memory, (4) Input Device, and (5) Output Device. For example, “What is the human equivalent to a Central Processing Unit”?

1) Brain
2) Short term memory
3) Long term memory
4) Ears, eyes, senses
5) Voice

### Exercise 9
How do you fix a “Syntax Error”?

Reading debug output. Reading code, making sure syntax is correct.
