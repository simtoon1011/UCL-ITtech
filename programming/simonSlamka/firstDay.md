## UCL programming course - the first day

- We'll be using Python during the first semester
- py4e.com - a site of an opensource book about python
- socratica - sarcastic videos about python
- programming and embedded systems will begin at 8:15
- co-op learning
- ohshitgit


## answers to the questions from chapter 1 of _the book_

### Exercise 1
- store info for the long term

### Exercise 2
- it's a sequence of instructions for the CPU (or the GPU in case of shaders, for example)

### Exercise 3
- The compiler takes the source code and translates it into machine code, resulting in a binary in a format that cannot be reverse-engineered (not easily anyway)

### Exercise 4
- the Python interpreter

### Exercise 5
- 'primt' --> 'print'

### Exercise 6
- Main mem (the RAM)

### Exercise 7
- 44

### Exercise 8
- the brain
- short-term mem
- long-term mem
- ears, eyes, tongue, skin
- voicebox

### Exercise 9
- Check the code. Make sure it's syntactically correct (the commands are existent and there are no misspellings)
