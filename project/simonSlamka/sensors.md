## Tasks
- list at least 5 sensors from the box we were given
- write about one of the sensors

## Answers

### task 1
- IR receiver
- Temp sensor
- Touch sensor
- Shock sensor
- Light sensor

### task 2
- The IR receiver is basically a diode sensitive to infrared light
(infrared part of the EM spectrum). It is used in devices that are controlled
with infrared remotes, such as a TV or a remote-controlled radio.
